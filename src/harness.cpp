#include "harness.h"
#include "json.h"
#include <atomic>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <ostream>
#include <shared_mutex>
#include <thread>
#include <variant>
#include <vector>

#ifdef NDEBUG // assert needs to work even in release mode
#undef NDEBUG
#endif
#include <cassert>

using std::chrono::steady_clock;

class BenchHarnessBase::ThreadCache final
{
  private:
    std::vector<std::thread> threads;
    std::shared_mutex state_lock;
    std::unique_lock<std::shared_mutex> locked_state;
    std::condition_variable_any cond_var;
    struct UnlockGuard final
    {
        std::shared_mutex &state_lock;
        UnlockGuard(std::shared_mutex &state_lock) : state_lock(state_lock)
        {
            state_lock.unlock();
        }
        ~UnlockGuard()
        {
            state_lock.lock();
        }
    };
    struct Task final
    {
        std::function<void()> fn;
    };
    struct ThreadState final
    {
        std::unique_ptr<Task> task;
        std::mutex mutex;
    };
    std::vector<std::shared_ptr<ThreadState>> states;
    bool shutting_down = false;
    std::atomic_size_t tasks_left_to_drain = 0;
    void add_thread()
    {
        auto thread_state = std::make_shared<ThreadState>();
        states.push_back(thread_state);
        threads.push_back(std::thread([this, thread_state]() {
            auto shared_lock = std::shared_lock(state_lock);
            while (true)
            {
                auto lock = std::unique_lock(thread_state->mutex);
                auto task = std::move(thread_state->task);
                lock.unlock();
                if (task)
                {
                    task->fn();
                    task.reset();
                    tasks_left_to_drain--;
                    cond_var.notify_all();
                    continue;
                }

                if (this->shutting_down)
                    return;

                cond_var.wait(shared_lock);
            }
        }));
    }

  public:
    ThreadCache()
    {
        locked_state = std::unique_lock(state_lock);
    }
    ThreadCache(const ThreadCache &) = delete;
    ThreadCache &operator=(const ThreadCache &) = delete;
    ~ThreadCache()
    {
        shutting_down = true;
        cond_var.notify_all();
        locked_state.unlock();
        for (auto &thread : threads)
        {
            thread.join();
        }
    }
    static std::shared_ptr<ThreadCache> get()
    {
        // weak so it's destroyed before returning from main()
        static std::weak_ptr<ThreadCache> static_thread_cache;

        std::shared_ptr<ThreadCache> thread_cache = static_thread_cache.lock();
        if (!thread_cache)
        {
            thread_cache = std::make_shared<ThreadCache>();
            static_thread_cache = thread_cache;
        }
        return thread_cache;
    }
    static std::shared_ptr<ThreadCache> get(BenchHarnessBase &bhb,
                                            std::uint32_t thread_count)
    {
        std::shared_ptr<ThreadCache> thread_cache = get();
        bhb.thread_cache = thread_cache;
        while (thread_cache->threads.size() < thread_count)
            thread_cache->add_thread();
        return thread_cache;
    }
    void drain()
    {
        while (tasks_left_to_drain > 0)
        {
            // unlocks state_lock, allowing all threads to proceed
            // simultaneously
            cond_var.wait(locked_state);
        }
    }
    template <typename Fn> void schedule_on(std::uint32_t thread_num, Fn fn)
    {
        auto lock = std::unique_lock(states[thread_num]->mutex);
        assert(!states[thread_num]->task);
        tasks_left_to_drain++;
        states[thread_num]->task = std::make_unique<Task>(Task{.fn = fn});
        cond_var.notify_all();
    }
};

struct WriteDuration final
{
    std::chrono::duration<double> dur;
    friend std::ostream &operator<<(std::ostream &os,
                                    const WriteDuration &wdur)
    {
        double dur = wdur.dur.count();
        if (!std::isfinite(dur) || std::fabs(dur) > 0.1)
        {
            os << dur << " sec";
        }
        else if (std::fabs(dur) > 0.1e-3)
        {
            os << dur * 1e3 << " ms";
        }
        else if (std::fabs(dur) > 0.1e-6)
        {
            os << dur * 1e6 << " us";
        }
        else if (std::fabs(dur) > 0.1e-9)
        {
            os << dur * 1e9 << " ns";
        }
        else if (std::fabs(dur) > 0.1e-12)
        {
            os << dur * 1e12 << " ps";
        }
        else
        {
            os << dur << " sec";
        }
        return os;
    }
};

struct BenchmarkResultInner final
{
    struct Result final
    {
        std::chrono::duration<double> total_dur;
        std::chrono::duration<double> iter_dur;
        operator JsonValue() const
        {
            return JsonValue::Object{
                {"total_dur", total_dur.count()},
                {"iter_dur", iter_dur.count()},
            };
        }
    };
    std::string name;
    JsonValue config_json;
    std::uint64_t iteration_count;
    Result average;
    std::vector<Result> threads;
};

struct BenchmarkResultImpl final : public BenchmarkResult
{
    BenchmarkResultInner inner;
    BenchmarkResultImpl(BenchmarkResultInner inner) : inner(std::move(inner))
    {
    }
    virtual void print() const override
    {
        std::cout << inner.name << ":\n";
        if (inner.threads.size() > 1)
        {
            for (std::size_t i = 0; i < inner.threads.size(); i++)
            {
                std::cout << "Thread #" << i << " took "
                          << WriteDuration{inner.threads[i].total_dur}
                          << " for " << inner.iteration_count
                          << " iterations -- "
                          << WriteDuration{inner.threads[i].iter_dur}
                          << "/iter.\n";
            }
        }
        std::cout << "Average elapsed time: "
                  << WriteDuration{inner.average.total_dur} << " for "
                  << inner.iteration_count << " iterations -- "
                  << WriteDuration{inner.average.iter_dur} << "/iter.\n"
                  << std::endl;
    }
    virtual operator JsonValue() const override
    {
        return JsonValue::Object{
            {"name", inner.name},
            {"config", inner.config_json},
            {"iteration_count", inner.iteration_count},
            {"average", inner.average},
            {"threads", inner.threads},
        };
    }
};

std::shared_ptr<BenchmarkResult> BenchHarnessBase::base_run(
    const Config &config, const std::string &name,
    void (*fn)(BenchHarnessBase *bench_harness_base,
               std::uint64_t iteration_count, std::uint32_t thread_num))
{

    std::uint32_t thread_count =
        config.thread_count.value_or(std::thread::hardware_concurrency());
    bool no_threads =
        thread_count == 0 || (thread_count == 1 && !config.thread_count);
    if (no_threads)
    {
        thread_count = 1;
    }

    std::vector<steady_clock::duration> elapsed(thread_count);
    auto run_base = [&](std::uint64_t iteration_count,
                        std::uint32_t thread_num) {
        auto start_time = steady_clock::now();
        fn(this, iteration_count, thread_num);
        auto end_time = steady_clock::now();
        elapsed[thread_num] = end_time - start_time;
    };
    auto run = [&](std::uint64_t iteration_count) {
        if (no_threads)
        {
            return run_base(iteration_count, 0);
        }
        auto thread_cache = ThreadCache::get(*this, thread_count);
        for (std::uint32_t thread_num = 0; thread_num < thread_count;
             thread_num++)
        {
            thread_cache->schedule_on(
                thread_num, [&run_base, iteration_count, thread_num]() {
                    run_base(iteration_count, thread_num);
                });
        }
        thread_cache->drain();
    };
    std::uint64_t iteration_count = 1;
    if (config.iteration_count)
    {
        iteration_count = *config.iteration_count;
        assert(iteration_count > 0);
        run(iteration_count);
    }
    else
    {
        while (true)
        {
            run(iteration_count);
            steady_clock::duration total_elapsed{};
            for (auto i : elapsed)
            {
                total_elapsed += i;
            }
            std::chrono::duration<double> target_average_elapsed =
                std::chrono::milliseconds(500);
            if (config.target_duration)
            {
                target_average_elapsed =
                    std::chrono::duration<double>(*config.target_duration);
            }
            if (total_elapsed > thread_count * target_average_elapsed ||
                iteration_count >= (1ULL << 63))
            {
                break;
            }
            iteration_count <<= 1;
        }
    }
    steady_clock::duration total_elapsed{};
    BenchmarkResultInner retval = {
        .name = name,
        .config_json = config,
        .iteration_count = iteration_count,
        .average = {},
        .threads = {},
    };
    for (std::uint32_t thread_num = 0; thread_num < thread_count; thread_num++)
    {
        total_elapsed += elapsed[thread_num];
        auto dur = std::chrono::duration<double>(elapsed[thread_num]);
        retval.threads.push_back({
            .total_dur = dur,
            .iter_dur = dur / iteration_count,
        });
    }
    auto total = std::chrono::duration<double>(total_elapsed);
    retval.average = {
        .total_dur = total / thread_count,
        .iter_dur = total / thread_count / iteration_count,
    };
    return std::make_shared<BenchmarkResultImpl>(retval);
}

std::shared_ptr<void> BenchHarnessBase::get_thread_cache()
{
    return ThreadCache::get();
}

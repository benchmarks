#pragma once

#include "../harness.h"

std::vector<Benchmark> powerpc64le_benchmarks(const Config &config);

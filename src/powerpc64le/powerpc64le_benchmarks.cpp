#include "powerpc64le_benchmarks.h"
#include <endian.h>

#if defined(__powerpc64__) && BYTE_ORDER == LITTLE_ENDIAN

std::vector<Benchmark> powerpc64le_benchmarks(const Config &config)
{
    std::vector<Benchmark> retval;
    // TODO: add powerpc64le benchmarks
    (void)config;
    return retval;
}

#else

std::vector<Benchmark> powerpc64le_benchmarks(const Config &)
{
    return {};
}

#endif

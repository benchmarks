#include "all_benchmarks.h"
#include "aarch64/aarch64_benchmarks.h"
#include "common/common_benchmarks.h"
#include "powerpc64le/powerpc64le_benchmarks.h"
#include "x86_64/x86_64_benchmarks.h"
#include <algorithm>
#include <iterator>

std::vector<Benchmark> all_benchmarks(const Config &config)
{
    std::vector<Benchmark> retval = common_benchmarks(config);
    {
        auto benchmarks = x86_64_benchmarks(config);
        std::move(benchmarks.begin(), benchmarks.end(),
                  std::back_inserter(retval));
    }
    {
        auto benchmarks = aarch64_benchmarks(config);
        std::move(benchmarks.begin(), benchmarks.end(),
                  std::back_inserter(retval));
    }
    {
        auto benchmarks = powerpc64le_benchmarks(config);
        std::move(benchmarks.begin(), benchmarks.end(),
                  std::back_inserter(retval));
    }
    return retval;
}

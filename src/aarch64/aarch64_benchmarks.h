#pragma once

#include "../harness.h"

std::vector<Benchmark> aarch64_benchmarks(const Config &config);

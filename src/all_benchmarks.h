#pragma once

#include "harness.h"

std::vector<Benchmark> all_benchmarks(const Config &config);

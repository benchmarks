#include "common_benchmarks.h"
#include "c11_atomics/c11_atomics_benchmarks.h"

std::vector<Benchmark> common_benchmarks(const Config &config)
{
    auto retval = c11_atomics_benchmarks(config);
    return retval;
}

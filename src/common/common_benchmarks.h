#pragma once

#include "../harness.h"

std::vector<Benchmark> common_benchmarks(const Config &config);

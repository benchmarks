#include "c11_atomics_benchmarks.h"
#include "internals.h"

std::vector<Benchmark> c11_atomics_benchmarks(const Config &config)
{
    using namespace c11_atomics;
    std::vector<Benchmark> benches;
    benchmarks<std::uint8_t>(benches, config);
    benchmarks<std::uint16_t>(benches, config);
    benchmarks<std::uint32_t>(benches, config);
    benchmarks<std::uint64_t>(benches, config);
    benchmarks<std::int8_t>(benches, config);
    benchmarks<std::int16_t>(benches, config);
    benchmarks<std::int32_t>(benches, config);
    benchmarks<std::int64_t>(benches, config);
    return benches;
}

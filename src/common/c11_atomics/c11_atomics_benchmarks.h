#pragma once

#include "../../harness.h"

std::vector<Benchmark> c11_atomics_benchmarks(const Config &config);

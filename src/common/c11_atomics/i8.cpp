#include "internals.h"

template void c11_atomics::benchmarks<std::int8_t>(
    std::vector<Benchmark> &benches, const Config &config);

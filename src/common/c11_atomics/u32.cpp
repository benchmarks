#include "internals.h"

template void c11_atomics::benchmarks<std::uint32_t>(
    std::vector<Benchmark> &benches, const Config &config);

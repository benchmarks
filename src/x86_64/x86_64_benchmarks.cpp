#include "x86_64_benchmarks.h"

#ifdef __x86_64__

std::vector<Benchmark> x86_64_benchmarks(const Config &config)
{
    std::vector<Benchmark> retval;
    // TODO: add x86_64 benchmarks
    (void)config;
    return retval;
}

#else

std::vector<Benchmark> x86_64_benchmarks(const Config &)
{
    return {};
}

#endif

The purpose of this benchmark is to compare the performance for
userspace tracing of SystemTap and LTTng-UST.
10 million events generated per thread, number of threads vary.  Each
event generates a time-stamp and contains a 4-byte integer value.

These four tests can be performed :
- UST in flight recorder mode
- UST writing the trace to disk
- SystemTap in flight recorder mode
- SystemTap writing the trace to disk

Dependencies
- LTTng-UST (tested on v0.11) : git://git.lttng.org/ust.git
- SystemTap (tested on v1.2-5 from debian packages)
- systemtap-sdt-dev for dtrace


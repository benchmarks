#!/bin/sh

# UST vs SystemTap scalability test
# This script can run 4 different tests :
# - UST in flight recorder mode
# - UST writing the trace to disk
# - SystemTap in flight recorder mode
# - SystemTap writing the trace to disk

# You need to be root to run the SystemTap tests because of the rmmod

BINARY=./tracepoint_benchmark
MODNAME=tpbench
REPORT=/tmp/testreport
TMPLOG=/tmp/testlog
WRAPPER=""
CLEANUP=""
STAP=stap
STAPTMP=/tmp/stapconsole
STAPPROBE=testutrace.stp

rm $REPORT 2>/dev/null

ust_flight_recorder() {
	# flight recorder, don't record trace to disk.
	# default buffer size is 4k
	echo -n "* UST Flight recorder : " | tee >> $REPORT
	export UST_AUTOCOLLECT=0
	export UST_OVERWRITE=1
	export UST_SUBBUF_NUM=16
	WRAPPER=usttrace
}

ust_disk() {
	# Collect traces to disk
	# default buffer size is 4k
	echo -n "* UST Write to disk : " | tee >> $REPORT
	export UST_AUTOCOLLECT=1
	export UST_OVERWRITE=0
	export UST_SUBBUF_NUM=16
	WRAPPER=usttrace
}

stap_flight_recorder() {
	echo -n "* SystemTap Flight recorder : " | tee >> $REPORT
	WRAPPER=""
	$STAP $STAPPROBE -F -m $MODNAME
}

stap_disk() {
	echo -n "* SystemTap Write to disk : " | tee >> $REPORT
	WRAPPER=""
	$STAP $STAPPROBE -o $STAPTMP -m $MODNAME &
	sleep 5
}

echo "Userspace tracing scalability test report" |tee >> $REPORT
case "$1" in
	ust_flight_recorder)
		TEST=ust_flight_recorder
		;;
	ust_disk)
		TEST=ust_disk
		;;
	stap_flight_recorder)
		TEST=stap_flight_recorder
		CLEANUP="rmmod $MODNAME 2>/dev/null"
		;;
	stap_disk)
		TEST=stap_disk
		CLEANUP="killall stapio 2>/dev/null"
		;;
	*)
		echo "Usage : $0 {ust_flight_recorder|ust_disk|stap_flight_recorder|stap_disk}"
		exit 1
		;;
esac

for nr_threads in 1 2 4 8; do
	echo "" | tee >> $REPORT
	echo Number of threads: $nr_threads | tee >> $REPORT
	echo -n "* Baseline : " | tee >> $REPORT

	eval $CLEANUP

	sync
	/usr/bin/time -f "%E" -o $TMPLOG $BINARY ${nr_threads}
	cat $TMPLOG >> $REPORT

	$TEST

	sync
	/usr/bin/time -f "%E" -o $TMPLOG $WRAPPER $BINARY ${nr_threads}
	cat $TMPLOG >> $REPORT
done

cat $REPORT

